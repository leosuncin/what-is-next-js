# What's Next.js? 💫 SSR sin esfuerzo 🎉

> Introducción al framework [Next.js](https://nextjs.org) por [Jaime Leonardo Suncin Cruz](mailto:jaime@suncin.me)

![Next.js](card.png)

## Instrucciones de uso

Primero es necesario clonar el repositorio

```bash
git clone https://gitlab.com/leosuncin/what-is-next-js.git
```

Cambiar al directorio recien clonado

```bash
cd what-is-next-js
```

Instalar las dependecias

```bash
npm install
```

Lanzar la presentación

```bash
npm start
```

## Presentación en linea

[http://horchatajs-nextjs.slides.suncin.me](http://url.suncin.me/horchatajs-8-slides)

## Demo

[https://horchatajs-nextjs-demo.suncin.now.sh/](http://url.suncin.me/horchata-8-demo)
