import Head from 'next/head';
import { Container } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

import Navigation from './navigation';
import Footer from './footer';

const Layout = ({ title, children }) => (
  <>
    <Head>
      <title>{title}</title>
      <link rel="shortcut icon" href="/static/favicon.png" type="image/png" />
    </Head>
    <Navigation />
    <Container as="main" role="main">
      {children}
    </Container>
    <Footer />
  </>
);

export default Layout;
