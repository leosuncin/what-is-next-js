import Link from 'next/link';
import { Navbar, Container, Nav } from 'react-bootstrap';
import { withRouter } from 'next/router';

const Navigation = ({ router }) => (
  <Navbar bg="dark" variant="dark" expand="md">
    <Container>
      <Link href="/" passHref>
        <Navbar.Brand>
          <img
            src="/static/logo.svg"
            width="30"
            height="30"
            className="d-inline-block align-top"
          />
          {' ACME'}
        </Navbar.Brand>
      </Link>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse className="justify-content-end" id="basic-navbar-nav">
        <Nav navbar>
          <Link href="/" passHref>
            <Nav.Link active={router.pathname === '/'}>Inicio</Nav.Link>
          </Link>
          <Link href="/blog" passHref>
            <Nav.Link active={router.pathname === '/blog'}>Blog</Nav.Link>
          </Link>
          <Link href="/contact-us" passHref>
            <Nav.Link active={router.pathname === '/contact-us'}>
              Contactenos
            </Nav.Link>
          </Link>
        </Nav>
      </Navbar.Collapse>
    </Container>
  </Navbar>
);

export default withRouter(Navigation);
