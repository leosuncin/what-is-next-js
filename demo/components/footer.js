import { Container } from 'react-bootstrap';
import Link from 'next/link';

const Footer = () => (
  <Container as="footer" className="mt-auto py-3">
    <p>
      &copy; 2019 ACME, Inc. · <Link href="/privacy"><a>Privacy</a></Link> ·&nbsp;
      <Link href="/terms"><a>Terms</a></Link>
    </p>
    <style jsx global>{`
      /* Sticky footer styles */
      body,
      body > div {
        display: flex;
        flex-direction: column;
        height: 100%;
        height: 100vh;
      }

      [role='main'] {
        flex-shrink: 0;
      }
    `}</style>
  </Container>
);

export default Footer;
