import { Container, Row, Col, Card } from 'react-bootstrap';
import Layout from '../components/layout';

const cards = [
  {
    image: 'https://picsum.photos/80',
    title: 'Capitalize',
    body:
      'Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion.',
  },
  {
    image: 'https://picsum.photos/80',
    title: 'Win-win',
    body:
      'Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway.',
  },
  {
    image: 'https://picsum.photos/80',
    title: 'Leverage',
    body:
      'Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition.',
  },
];

const IndexPage = () => (
  <Layout title="ACME - Página de inicio">
    <div className="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
      <div className="col-md-5 p-lg-5 mx-auto my-5">
        <h1 className="display-4 font-weight-normal">Bienvenidos a ACME</h1>
        <p className="lead font-weight-normal">
          Así como lo vió en HorchataJS v8.0.0
        </p>
        <a
          className="btn btn-outline-secondary"
          href="https://github.com/horchatajs/charlas/issues/49"
          target="_blank"
          referrerPolicy="no-referrer"
        >
          Conocer más
        </a>
      </div>
      <div className="product-device shadow-sm d-none d-md-block" />
      <div className="product-device product-device-2 shadow-sm d-none d-md-block" />
    </div>
    <hr className="featurette-divider" />
    <Container>
      <Row>
        <Col md={7}>
          <h2 className="featurette-heading">
            Podcasting operational heading.{' '}
            <span className="text-muted">It’ll blow your mind.</span>
          </h2>
          <p className="lead">
            Podcasting operational change management inside of workflows to
            establish a framework. Taking seamless key performance indicators
            offline to maximise the long tail. Keeping your eye on the ball
            while performing a deep dive on the start-up mentality to derive
            convergence on cross-platform integration.
          </p>
        </Col>
        <Col md={5}>
          <img src="https://picsum.photos/400" />
        </Col>
      </Row>
    </Container>
    <hr className="featurette-divider" />
    <Container>
      <Row>
        {cards.map(card => (
          <Col key={card.title} md={4}>
            <Card className="mb-4 shadow-sm">
              <Card.Img variant="top" src={card.image} />
              <Card.Body>
                <Card.Title as="h1">{card.title}</Card.Title>
                <Card.Text>{card.body}</Card.Text>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
    <hr className="featurette-divider" />

    <style jsx>{`
      .product-device {
        position: absolute;
        right: 10%;
        bottom: -30%;
        width: 300px;
        height: 540px;
        background-color: #333;
        border-radius: 21px;
        -webkit-transform: rotate(30deg);
        transform: rotate(30deg);
      }

      .product-device::before {
        position: absolute;
        top: 10%;
        right: 10px;
        bottom: 10%;
        left: 10px;
        content: '';
        background-color: rgba(255, 255, 255, 0.1);
        border-radius: 5px;
      }

      .product-device-2 {
        top: -25%;
        right: auto;
        bottom: 0;
        left: 5%;
        background-color: #e5e5e5;
      }

      .featurette-divider {
        margin: 5rem 0; /* Space out the Bootstrap <hr> more */
      }

      /* Thin out the marketing headings */
      .featurette-heading {
        font-weight: 300;
        line-height: 1;
        letter-spacing: -0.05rem;
      }

      @media (min-width: 40em) {
        .featurette-heading {
          font-size: 50px;
        }
      }

      @media (min-width: 62em) {
        .featurette-heading {
          margin-top: 7rem;
        }
      }
    `}</style>
  </Layout>
);

export default IndexPage;
