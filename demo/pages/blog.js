import 'isomorphic-fetch';
import { Jumbotron, Col, Container, Row } from 'react-bootstrap';
import Link from 'next/link';
import ReactMarkdown from 'react-markdown';

import Layout from '../components/layout';

const BlogPage = ({ posts, quote }) => (
  <Layout title="ACME - Blog">
    <Jumbotron fluid className="p-4 p-md-5">
      <Col md={8}>
        <h1 className="display-4 font-italic">
          Las últimos anuncios y publicaciones
        </h1>
        <p className="lead my-3">
          Django information wants to be free retweet Gutenberg parenthesis a
          giant stack of newspapers that you'll never read the audience knows
          more than I do content is king advertising dead trees.
        </p>
      </Col>
    </Jumbotron>
    <Container>
      <Row>
        <Col md={8}>
          {posts.map(post => (
            <>
              <div key={post.slug} className="blog-post">
                <h2 className="blog-post-title">{post.title}</h2>
                <p className="blog-post-meta">
                  {new Date(post.createdAt).toLocaleDateString('es-SV', {
                    month: 'long',
                    day: '2-digit',
                    year: 'numeric',
                  })}{' '}
                  por <strong>{post.author}</strong>
                </p>
                <ReactMarkdown source={post.abstract} />
                <Link href={`/post/${post.slug}`}>
                  <a className="stretched-link">Continuar leyendo</a>
                </Link>
              </div>
              <hr />
            </>
          ))}
        </Col>
        <Col as="aside" md={4} className="blog-sidebar">
          <div className="p-4 mb-3 bg-light rounded">
            <h4 className="font-italic">{quote.author}</h4>
            <p className="mb-0">{quote.body}</p>
          </div>
        </Col>
      </Row>
    </Container>
    <style jsx>{`
      .blog-post {
        margin-bottom: 4rem;
      }

      .blog-post-title {
        margin-bottom: 0.25rem;
        font-size: 2.5rem;
      }

      .blog-post-meta {
        margin-bottom: 1.25rem;
        color: #999;
      }
    `}</style>
  </Layout>
);

BlogPage.getInitialProps = async () => {
  const [respPosts, respQuote] = await Promise.all([
    fetch(`${process.env.BASE_URL}/api/posts`),
    fetch(`${process.env.BASE_URL}/api/quote`),
  ]);
  const [posts, quote] = await Promise.all([
    respPosts.json(),
    respQuote.json(),
  ]);

  return { posts, quote };
};

export default BlogPage;
