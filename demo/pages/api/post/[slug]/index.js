import posts from '../../../../data/posts.json';

export default (req, res) => {
  const { slug } = req.query;
  const [post] = posts.filter(post => post.slug === slug);

  if (post) {
    delete post.abstract;
    post.createdAt = new Date();
  }

  res.json(post);
};
