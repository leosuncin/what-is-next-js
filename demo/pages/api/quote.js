import 'isomorphic-fetch';

const quoteHandler = async (req, res) => {
  const resp = await fetch('https://favqs.com/api/qotd');
  const json = await resp.json();
  const { url, author, body } = json.quote;

  res.json({ url, author, body });
};

export default quoteHandler;
