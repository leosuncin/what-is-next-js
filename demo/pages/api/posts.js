import posts from '../../data/posts.json';

const listPostHandle = (req, res) => {
  res.json(
    posts.map(post => {
      delete post.body;
      post.createdAt = new Date();

      return post;
    }),
  );
};

export default listPostHandle;
