import { useState } from 'react';
import Layout from '../components/layout';
import { Row, Col, Card, Form, Button } from 'react-bootstrap';

const ContactUsPage = () => {
  const [isSending, setSending] = useState(false);
  const [error, setError] = useState();
  const handleSubmit = event => {
    event.preventDefault();
    const form = event.target;
    const data = Array.from(form.elements)
      .filter(el => el.hasAttribute('name'))
      .map(el => ({
        [el.name]: el.value,
      }))
      .reduce((prev, curr) => ({ ...prev, ...curr }), {});

    setSending(true);
    setError();

    fetch(`${process.env.BASE_URL}/api/send-email`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then(resp => resp.json())
      .then(json => {
        setSending(false);
      })
      .catch(err => {
        setSending(false);
        setError(err.message ? err.message : err);
      });
  };
  return (
    <Layout title="Contactenos">
      <Row className="h-100 justify-content-center align-items-center">
        <Col md={5} className="p-5">
          <Card>
            <Card.Header>Formulario de contacto</Card.Header>
            <Card.Body>
              <Form onSubmit={handleSubmit}>
                <Form.Group
                  controlId="fullname"
                  as="fieldset"
                  disabled={isSending}
                >
                  <Form.Label>Nombre completo</Form.Label>
                  <Form.Control name="fullname" required />
                </Form.Group>
                <Form.Group
                  controlId="subject"
                  as="fieldset"
                  disabled={isSending}
                >
                  <Form.Label>Asunto</Form.Label>
                  <Form.Control name="subject" required minLength="2" />
                </Form.Group>
                <Form.Group
                  controlId="email"
                  as="fieldset"
                  disabled={isSending}
                >
                  <Form.Label>Correo electrónico</Form.Label>
                  <Form.Control type="email" name="email" required />
                </Form.Group>
                <Form.Group
                  controlId="message"
                  as="fieldset"
                  disabled={isSending}
                >
                  <Form.Label>Mensaje</Form.Label>
                  <Form.Control
                    as="textarea"
                    name="message"
                    rows="5"
                    required
                  />
                </Form.Group>
                <Button variant="primary" type="submit" disabled={isSending}>
                  {isSending ? 'Enviando mensaje' : 'Enviar mensaje'}
                </Button>
                {error && <p className="text-alert">{error}</p>}
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Layout>
  );
};

export default ContactUsPage;
