import 'isomorphic-fetch';
import { Image, Col, Container, Row } from 'react-bootstrap';
import ReactMarkdown from 'react-markdown';

import Layout from '../../../components/layout';

const BlogPage = ({ post }) => (
  <Layout title={post.title}>
    <Image src="https://picsum.photos/1500/600" fluid />
    <Container>
      <Row>
        <Col>
          <div className="blog-post">
            <h2 className="blog-post-title">{post.title}</h2>
            <p className="blog-post-meta">
              {new Date(post.createdAt).toLocaleDateString('es-SV', {
                month: 'long',
                day: '2-digit',
                year: 'numeric',
              })}{' '}
              por <strong>{post.author}</strong>
            </p>
            <ReactMarkdown source={post.body} />
          </div>
        </Col>
      </Row>
    </Container>
    <style jsx>{`
      .blog-post {
        margin-bottom: 4rem;
      }

      .blog-post-title {
        margin-bottom: 0.25rem;
        font-size: 2.5rem;
      }

      .blog-post-meta {
        margin-bottom: 1.25rem;
        color: #999;
      }
    `}</style>
  </Layout>
);

BlogPage.getInitialProps = async ({ query }) => {
  const resp = await fetch(`${process.env.BASE_URL}/api/post/${query.slug}`);
  const post = await resp.json();

  return { post };
};

export default BlogPage;
