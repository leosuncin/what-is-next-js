const withCSS = require('@zeit/next-css');

module.exports = withCSS({
  webpack: config => {
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: 'empty',
    };

    return config;
  },
  env: {
    BASE_URL: process.env.BASE_URL || 'http://localhost:3000',
  },
});
